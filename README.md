#  Royal Alchemi's Second Life Texture Change Scripter Version 1.0.0 #

##  	Introduction ##
The Texture Change Scripter is a simple program to make creator's and/or their programmer's lives easier.  While texture change scripts aren't complicated they can take time, better used on more pressing projects. With this program you can generate the code for the GUI and the object that will have it's texture(s) changed, and allows you to break that down into “parts” too allow for a higher level of customization by your customers.

##  	Starting up the Program ##
If you can't run the program by double clicking it, you either downloaded (from a non-official source) a bad file, your file got corrupted during the download, or more likely then not you don't have Java or it's out of date. You can get the latest version of Java here https://java.com/en/download/index.jsp

##  	Using the Program ##

###  	Tree ###

Here you can select the part or option you wish to edit, add options to, or delete. Each item has a icon that corresponds to it's type

###  	Options ###

**	Texture Name**: Here you put the name as you want it too show up in the options to the user.

**	Texture UUID**: Here you put the UUID of the texture.

**	Enable Tinting**: Turns on or off tinting for this option.

**	Tint**: The RBG values of the tint color. Do not use the standard 0-255 values use LSL's 0.0-1.0 values.

###  	Parts ###

**	Part Name**: The name of the part, this is only important if you have more then one part.

**	Link Number(s)**: The link number(s) that will be effected by that part.

**	Face(s)**: The face number(s) that will be effected by that part.

**	Constant**: If this is checked each time a texture/color is changed the options under this part will be applied.

###  	Part Links ###