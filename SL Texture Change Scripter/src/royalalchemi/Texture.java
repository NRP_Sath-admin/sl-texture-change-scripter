package royalalchemi;

public class Texture {
	private String name;
	private String UUID;
	private double R = 1.0, B = 1.0, G = 1.0;
	private boolean tint;
	
	Texture(String name, String UUID, boolean tint, String r, String b, String g){
		this.setName(name);
		this.setUUID(UUID);
		this.setRValue(Double.parseDouble(r));
		this.setBValue(Double.parseDouble(b));
		this.setGValue(Double.parseDouble(g));
		this.setTint(tint);
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString(){
		return this.getName();
	}

	public double getRValue() {
		return R;
	}

	public void setRValue(double r) {
		R = r;
	}

	public double getBValue() {
		return B;
	}

	public void setBValue(double b) {
		B = b;
	}

	public double getGValue() {
		return G;
	}

	public void setGValue(double g) {
		G = g;
	}

	public boolean isTint() {
		return tint;
	}

	public void setTint(boolean tint) {
		this.tint = tint;
	}
}
