package royalalchemi;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;

import net.miginfocom.swing.MigLayout;

import javax.swing.Icon;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import royalalchemi.util.FileSDs;
import components.DynamicTree;

import javax.swing.JList;
import javax.swing.JComboBox;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.border.LineBorder;

public class Main {

	private JFrame frmraSlTexture;
	private JTextField txfTextureName;
	private JTextField txfTextureUUID;
	
	private FileSDs fileSDs;
	
	private ArrayList<Part> partsList = new ArrayList<>();
	
	private JTextField txfPartLinkNumbers;
	private JTextField txfPartFaces;
	private JTextField txfPartName;
	private DynamicTree dynamicTree;
	
	private final String VERSION = "1.0.0";
	private int n;
	private JComboBox<Part> cbxParts;
	private JList<String> listLinkedParts;
	private Icon iconPart;
	private Icon iconOption;
	private Icon iconHUD;
	private JCheckBox chckbxConstant;
	private JTextField txfR;
	private JTextField txfB;
	private JTextField txfG;
	private JPanel pnlPreviewColor;
	private JCheckBox chckbxEnableTinting;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmraSlTexture.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		try{
			// Set System L&F
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch (UnsupportedLookAndFeelException e){
			// handle exception
		}catch (ClassNotFoundException e){
			// handle exception
		}catch (InstantiationException e){
			// handle exception
		}catch (IllegalAccessException e){
			// handle exception
		}
        
		iconHUD = new ImageIcon(DynamicTree.class.getResource("/royalalchemi/img/IconHUD.png"));
        iconPart = new ImageIcon(DynamicTree.class.getResource("/royalalchemi/img/IconPart.png"));
        iconOption = new ImageIcon(DynamicTree.class.getResource("/royalalchemi/img/IconOption.png"));
        
		fileSDs = new FileSDs("C:\\TextureChangeScripter\\");
		if(!new File(fileSDs.getCurrentDirectory() + "\\config.cfg").isFile()){
			try {
				n = 10000;
				fileSDs.serializeObject(n, "config", ".cfg");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}else{
			try {
				n = (int) fileSDs.deserializeObject(new File(fileSDs.getCurrentDirectory() + "\\config.cfg"));
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		partsList.add(new Part("Part").setFaces("-1").setLinks("-1"));
		
		frmraSlTexture = new JFrame();
		frmraSlTexture.setIconImage(Toolkit.getDefaultToolkit().getImage(Main.class.getResource("/royalalchemi/img/Icon.png")));
		frmraSlTexture.setTitle(".:RA:. SL Texture Change Scripter [" + VERSION + "]");
		frmraSlTexture.setBounds(100, 100, 760, 725);
		frmraSlTexture.setMinimumSize(new Dimension(718,200));
		frmraSlTexture.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmraSlTexture.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		frmraSlTexture.getContentPane().add(scrollPane_2);
		
		JPanel panel_5 = new JPanel();
		scrollPane_2.setViewportView(panel_5);
		panel_5.setLayout(new MigLayout("", "[236.00px,fill][456px,grow,fill]", "[585px,grow,fill]"));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_5.add(scrollPane, "cell 0 0,alignx left,aligny center");
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		dynamicTree = new DynamicTree();
		dynamicTree.setToolTipText("All parts and their texture options.");
		dynamicTree.addTreeSelectionListener(new TreeSelectionListener() {
		    public void valueChanged(TreeSelectionEvent e) {
		        DefaultMutableTreeNode node = (DefaultMutableTreeNode)dynamicTree.getLastSelectedPathComponent();
		        if (node == null){
		        	return;
		        }else if(dynamicTree.selectedValue().getPath().length <= 1){
		        	dynamicTree.setSelection(null);
		        	return;
		        }
		        
		        txfPartName.setText(dynamicTree.selectedValue().getPath()[1].toString());
		        cbxParts.removeAllItems();
				DefaultListModel<String> model = new DefaultListModel<>();
		        for(Part p : partsList){
	        		//Find selected part in partsList
	        		if(p.getName().equals(dynamicTree.selectedValue().getPath()[1].toString())){
	        			txfPartLinkNumbers.setText(p.getLinks().toString().replace("[", "").replace("]", ""));
	        			txfPartFaces.setText(p.getFaces().toString().replace("[", "").replace("]", ""));
	        			chckbxConstant.setSelected(p.isConstant());
	        		}
	    			if(!dynamicTree.selectedValue().getPath()[1].toString().equals(p.getName())){
						for(String s: p.getChildren()){
							model.addElement(s);
						}
	    				cbxParts.addItem(p);
	    			}
		        }
				listLinkedParts.setModel(model);
		        
		        if(dynamicTree.selectedValue().getPath().length > 2){
		        	
		        	txfTextureName.setText(dynamicTree.selectedValue().getPath()[2].toString());
		        	
		        	for(Part p : partsList){
		        		//Find selected part in partsList
		        		if(p.getName().equals(dynamicTree.selectedValue().getPath()[1].toString())){
		        			txfPartLinkNumbers.setText(p.getLinks().toString().replace("[", "").replace("]", ""));
		        			txfPartFaces.setText(p.getFaces().toString().replace("[", "").replace("]", ""));
		        			for(Texture t : p.getOptions()){
		        				//Find selected texture in selected part.
		        				if(t.getName().equals(dynamicTree.selectedValue().getPath()[2].toString())){
		        					txfTextureUUID.setText(t.getUUID());
		        					chckbxEnableTinting.setSelected(t.isTint());
		        					txfR.setText(Double.toString(t.getRValue()));
		        					txfB.setText(Double.toString(t.getBValue()));
		        					txfG.setText(Double.toString(t.getGValue()));
		        					updatePreviewColor();
		        				}
		        			}
		        		}

		        	}
		        }
		    }
		   });
		scrollPane.setViewportView(dynamicTree);
		dynamicTree.setCellRenderer(new DefaultTreeCellRenderer() {
			private static final long serialVersionUID = -1688639662161748541L;
            @Override
            public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean isLeaf, int row, boolean focused) {
                Component c = super.getTreeCellRendererComponent(tree, value, selected, expanded, isLeaf, row, focused);
                int level = ((DefaultMutableTreeNode)value).getLevel();
                if(level == 0){
                	setIcon(iconHUD);
                }
                if(level == 1){
                	setIcon(iconPart);
                }
                if(level == 2){
                	setIcon(iconOption);
                }
                return c;
            }
        });
		
		JPanel panel = new JPanel();
		panel_5.add(panel, "cell 1 0,growx,aligny top");
		panel.setBorder(null);
		panel.setLayout(new MigLayout("", "[grow]", "[][][][][][grow,fill][bottom]"));
		
		JLabel lblOptions = new JLabel("Options");
		panel.add(lblOptions, "cell 0 0");
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.add(panel_2, "cell 0 1,grow");
		panel_2.setLayout(new MigLayout("", "[grow]", "[][][][][][][][][]"));
		
		JLabel lblTextureName = new JLabel("Texture Name");
		panel_2.add(lblTextureName, "cell 0 0,alignx left");
		
		txfTextureName = new JTextField();
		txfTextureName.setToolTipText("Texture name.");
		panel_2.add(txfTextureName, "cell 0 1,growx");
		txfTextureName.setColumns(10);
		
		JLabel lblTextureUuid = new JLabel("Texture UUID");
		panel_2.add(lblTextureUuid, "cell 0 2,alignx left");
		
		txfTextureUUID = new JTextField();
		txfTextureUUID.setToolTipText("Texture UUID. Right click on a texture in SL and click \"Copy assets UUID\" to get this.");
		panel_2.add(txfTextureUUID, "cell 0 3,growx");
		txfTextureUUID.setColumns(10);
		
		JButton btnAddOption = new JButton("Add/Update Option");
		btnAddOption.setToolTipText("Add or update a texture.");
		btnAddOption.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TreePath path = dynamicTree.selectedValue();
				if(path != null){
					String name = path.getPath()[1].toString();
					for(Part p : partsList){
						if(p.getName().equals(name)){
							System.out.println("Found");
							p.addOption(new Texture(txfTextureName.getText(), txfTextureUUID.getText(), chckbxEnableTinting.isSelected(), txfR.getText(), txfB.getText(), txfG.getText()));
						}
					}
					updateList();
				}
			}
		});
		
		chckbxEnableTinting = new JCheckBox("Enable Tinting");
		chckbxEnableTinting.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txfR.setEnabled(chckbxEnableTinting.isSelected());
				txfB.setEnabled(chckbxEnableTinting.isSelected());
				txfG.setEnabled(chckbxEnableTinting.isSelected());
			}
		});
		panel_2.add(chckbxEnableTinting, "cell 0 4");
		
		JLabel lblTint = new JLabel("Tint");
		panel_2.add(lblTint, "cell 0 5");
		
		JLabel lblR = new JLabel("R");
		lblR.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblR.setForeground(Color.RED);
		panel_2.add(lblR, "flowx,cell 0 6");
		
		pnlPreviewColor = new JPanel();
		pnlPreviewColor.setBorder(new LineBorder(new Color(0, 0, 0)));
		pnlPreviewColor.setBackground(Color.WHITE);
		panel_2.add(pnlPreviewColor, "cell 0 7,grow");
		panel_2.add(btnAddOption, "flowx,cell 0 8,alignx left");
		
		JButton btnDeleteOption = new JButton("Delete Option");
		btnDeleteOption.setToolTipText("Delete selected texture.");
		btnDeleteOption.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(dynamicTree.selectedValue() != null){
					for(Part p : partsList){
						if(p.getName().equals(dynamicTree.selectedValue().getPath()[1].toString())){
							p.removeOption(dynamicTree.selectedValue().getPath()[2].toString());
						}
					}
					updateList();
				}
			}
		});
		panel_2.add(btnDeleteOption, "cell 0 8,alignx left");
		
		txfR = new JTextField();
		txfR.setEnabled(false);
		txfR.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				updatePreviewColor();
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				updatePreviewColor();
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
			}
		});
		txfR.setText("1.0");
		panel_2.add(txfR, "cell 0 6");
		txfR.setColumns(10);
		
		JLabel lblG = new JLabel("G");
		lblG.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblG.setForeground(Color.GREEN);
		panel_2.add(lblG, "cell 0 6");
		
		txfG = new JTextField();
		txfG.setEnabled(false);
		txfG.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				updatePreviewColor();
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				updatePreviewColor();
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
			}
		});
		txfG.setText("1.0");
		panel_2.add(txfG, "cell 0 6");
		txfG.setColumns(10);
		
		JLabel lblB = new JLabel("B");
		lblB.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblB.setForeground(Color.BLUE);
		panel_2.add(lblB, "cell 0 6");
		
		txfB = new JTextField();
		txfB.setEnabled(false);
		txfB.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				updatePreviewColor();
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				updatePreviewColor();
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
			}
		});
		txfB.setText("1.0");
		panel_2.add(txfB, "cell 0 6");
		txfB.setColumns(10);
		
		JLabel lblParts = new JLabel("Parts");
		panel.add(lblParts, "cell 0 2");
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, "cell 0 3,grow");
		panel_1.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_1.setLayout(new MigLayout("", "[grow]", "[][][][][][][][]"));
		
		JLabel lblPartName = new JLabel("Part Name");
		panel_1.add(lblPartName, "cell 0 0");
		
		txfPartName = new JTextField();
		txfPartName.setToolTipText("Name of the part this isn't important if there's only one part being changed.");
		panel_1.add(txfPartName, "cell 0 1,growx");
		txfPartName.setColumns(10);
		
		JLabel lblLinkNumbers = new JLabel("Link Number(s)");
		panel_1.add(lblLinkNumbers, "cell 0 2");
		
		txfPartLinkNumbers = new JTextField();
		txfPartLinkNumbers.setToolTipText("Seperate link numbers with ,");
		panel_1.add(txfPartLinkNumbers, "cell 0 3,growx");
		txfPartLinkNumbers.setColumns(10);
		
		JLabel lblFaces = new JLabel("Face(s)");
		panel_1.add(lblFaces, "cell 0 4");
		
		txfPartFaces = new JTextField();
		txfPartFaces.setToolTipText("Seperate face numbers with ,");
		panel_1.add(txfPartFaces, "cell 0 5,growx");
		txfPartFaces.setColumns(10);
		
		JButton btnAddPart = new JButton("Add/Update Part");
		btnAddPart.setToolTipText("Add or update a part.");
		btnAddPart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = txfPartName.getText();
				for(Part p : partsList){
					if(p.getName().equals(name)){
						p.setFaces(txfPartFaces.getText());
						p.setLinks(txfPartLinkNumbers.getText());
						p.setConstant(chckbxConstant.isSelected());
						updateList();
						return;
					}
				}
				partsList.add(new Part(name).setFaces(txfPartFaces.getText()).setLinks(txfPartLinkNumbers.getText()).setConstant(chckbxConstant.isSelected()));
				updateList();
			}
		});
		
		chckbxConstant = new JCheckBox("Constant");
		chckbxConstant.setToolTipText("Constant parts are always set each time the texture is changed.");
		panel_1.add(chckbxConstant, "cell 0 6");
		panel_1.add(btnAddPart, "flowx,cell 0 7,alignx left");
		
		JButton btnDeletePart = new JButton("Delete Part");
		btnDeletePart.setToolTipText("Delete selected part.");
		btnDeletePart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(dynamicTree.selectedValue() != null){
					Part temp = null;
					for(Part p : partsList){
						if(p.getName().equals(dynamicTree.selectedValue().getPath()[1].toString())){
							temp = p;
						}
					}
					partsList.remove(temp);
					updateList();
				}
			}
		});
		panel_1.add(btnDeletePart, "cell 0 7,alignx left");
		
		JButton btnCompileScript = new JButton("Compile Script");
		btnCompileScript.setToolTipText("Opens a window with the code.");
		btnCompileScript.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				compileScript();
			}
		});
		
		JLabel lblPartLinks = new JLabel("Part Links");
		panel.add(lblPartLinks, "cell 0 4");
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.add(panel_3, "cell 0 5,grow");
		panel_3.setLayout(new MigLayout("", "[grow,fill][]", "[grow]"));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panel_3.add(scrollPane_1, "cell 0 0,grow");
		
		listLinkedParts = new JList<>();
		scrollPane_1.setViewportView(listLinkedParts);
		
		JPanel panel_4 = new JPanel();
		panel_3.add(panel_4, "cell 1 0,grow");
		panel_4.setLayout(new MigLayout("", "[grow]", "[][][][]"));
		
		cbxParts = new JComboBox<Part>();
		panel_4.add(cbxParts, "cell 0 0,growx");
		
		JButton btnAddChild = new JButton("Add Child");
		btnAddChild.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = cbxParts.getSelectedItem().toString();
				for(Part p : partsList){
					if(p.getName().equals(name)){
						p.addChild(cbxParts.getSelectedItem().toString());
					}
				}
				updateList();
			}
		});
		panel_4.add(btnAddChild, "cell 0 1,growx");
		
		JButton btnRemoveChild = new JButton("Remove Child");
		btnRemoveChild.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = txfPartName.getText();
				if(dynamicTree.selectedValue() != null){
					for(Part p : partsList){
						if(p.getName().equals(name)){
							p.removeChild((String)cbxParts.getSelectedItem());
						}
					}
				}
			}
		});
		panel_4.add(btnRemoveChild, "cell 0 2,growx");
		
		JButton btnRemoveAllChildren = new JButton("Remove All Children");
		panel_4.add(btnRemoveAllChildren, "cell 0 3,growx");
		panel.add(btnCompileScript, "flowx,cell 0 6,alignx left,aligny bottom");
		
		JMenuBar menuBar = new JMenuBar();
		frmraSlTexture.setJMenuBar(menuBar);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(frmraSlTexture,
					    "Developed by Brian Milligan (aka. Nai Alchemi)\n"
					    + "Program Version: " + VERSION
					    + "\nMade using Eclipse and WindowBuilder Pro GUI Designer 8.0\n"
					    + "Unless otherwise stated this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0\n"
					    + "International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.\n"
					    + "If you paid for this demand a refund from the vendor immediately, this is a free program and is not licensed for sale.",
					    "About",
					    JOptionPane.PLAIN_MESSAGE);
			}
		});
		mnHelp.add(mntmAbout);
		updateList();
	}
	
	private void updatePreviewColor() {
		try{
			pnlPreviewColor.setBackground(new Color(Float.parseFloat(txfR.getText()), Float.parseFloat(txfG.getText()), Float.parseFloat(txfB.getText())));
		}catch(NumberFormatException e1){
			
		}catch(IllegalArgumentException e2){
			
		}
	}

	private void updateList(){
		TreePath oldPath = dynamicTree.selectedValue();
		dynamicTree.clear();
		cbxParts.removeAllItems();
		for(Part p : partsList){
			DefaultMutableTreeNode temp = dynamicTree.addObject(p);
			for(Texture t : p.getOptions()){
				dynamicTree.addObject(temp, t);
			}
		}
		dynamicTree.expandAllNodes();
		dynamicTree.setSelection(oldPath);
	}

	private void compileScript(){
		String[] CODE = new String[2];
		CODE = new CodeGenerator().GenerateCode(partsList, n);
		new ExportViewer(CODE[0], CODE[1]).setVisible(true);
		n += partsList.size();
		try {
			fileSDs.serializeObject(n, "config", ".cfg");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
