package royalalchemi;

import java.util.ArrayList;

public class Part {
	private String 				name;
	private ArrayList<Integer>	faces		=	new ArrayList<>();
	private ArrayList<Integer>	links		=	new ArrayList<>();
	private ArrayList<Texture>	options		=	new ArrayList<>();
	private ArrayList<String>	children	=	new ArrayList<>();
	private boolean				constant	=	false;
	
	public Part(String name){
		this.setName(name);
	}
	
	public boolean addOption(Texture option){
		for(Texture t : options){
			if(t.getName().equals(option.getName())){
				t.setUUID(option.getUUID());
				t.setTint(option.isTint());
				t.setRValue(option.getRValue());
				t.setBValue(option.getBValue());
				t.setGValue(option.getGValue());
				return true;
			}
		}
		return this.options.add(option);
	}
	
	public boolean removeOption(String name){
		for(Texture t : options){
			if(t.getName().equals(name)){
				return options.remove(t);
			}
		}
		return false;
	}
	public Texture getOption(int index){
		return this.options.get(index);
	}

	public ArrayList<Integer> getFaces() {
		return this.faces;
	}

	public Part setFaces(String faces) {
		System.out.println(faces.split(","));
		this.faces.clear();
		for(String s : faces.split(",")){
			if(s.equals("ALL_SIDES")){
				s = "-1";
			}
			this.faces.add(Integer.parseInt(s.trim()));
		}
		return this;
	}

	public ArrayList<Integer> getLinks() {
		return this.links;
	}

	public Part setLinks(String links) {
		System.out.println(links.split(","));
		this.links.clear();
		for(String s : links.split(",")){
			s = s.trim();
			if(s.equals("LINK_ROOT")){
				this.links.add(1);
			}else if(s.equals("LINK_SET")){
				this.links.add(-1);
			}else if(s.equals("LINK_ALL_OTHERS")){
				this.links.add(-2);
			}else if(s.equals("LINK_ALL_CHILDREN")){
				this.links.add(-3);
			}else if(s.equals("LINK_THIS")){
				this.links.add(-4);
			}else{
				this.links.add(Integer.parseInt(s));
			}
		}
		return this;
	}

	public String getName() {
		if(this.constant){
			return "[" + this.name + "]";
		}else{
			return this.name;
		}
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString(){
		return this.getName();
	}

	public ArrayList<Texture> getOptions() {
		return this.options;
	}

	public ArrayList<String> getChildren() {
		return this.children;
	}

	public void addChild(String child) {
		for(String s:this.children){
			if(s.equals(child)){
				return;
			}
		}
		this.children.add(child);
	}
	
	public void removeChild(String child){
		this.children.remove(child);
	}

	public boolean isConstant() {
		return this.constant;
	}

	public Part setConstant(boolean constant) {
		this.constant = constant;
		return this;
	}
}
