package royalalchemi;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ExportViewer extends JFrame {
	private static final long serialVersionUID = 884752904019025391L;
	private JPanel contentPane;
	private JTextArea txaObjectCode;
	private JTextArea txaHUDCode;

	/**
	 * Create the frame.
	 */
	public ExportViewer(String hudCode, String objectCode) {
		setType(Type.POPUP);
		setTitle("Compiled Code");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 626, 416);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow][grow]", "[][233px,grow][bottom]"));
		
		JLabel lblHudCode = new JLabel("HUD Code");
		contentPane.add(lblHudCode, "cell 0 0");
		
		JLabel lblObjectCode = new JLabel("Object Code");
		contentPane.add(lblObjectCode, "cell 1 0");
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scrollPane, "cell 0 1,grow");
		
		txaHUDCode = new JTextArea();
		txaHUDCode.setEditable(false);
		txaHUDCode.setText(hudCode);
		scrollPane.setViewportView(txaHUDCode);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scrollPane_1, "cell 1 1,grow");
		
		txaObjectCode = new JTextArea();
		txaObjectCode.setEditable(false);
		txaObjectCode.setText(objectCode);
		scrollPane_1.setViewportView(txaObjectCode);
		
		JButton btnCopyHudCode = new JButton("Copy HUD Code");
		btnCopyHudCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				writeToClipboard(txaHUDCode.getText());
			}
		});
		contentPane.add(btnCopyHudCode, "cell 0 2,growx,aligny bottom");
		
		JButton btnCopyObjectCode = new JButton("Copy Object Code");
		btnCopyObjectCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				writeToClipboard(txaObjectCode.getText());
			}
		});
		contentPane.add(btnCopyObjectCode, "cell 1 2,growx,aligny bottom");
	}
	public static void writeToClipboard(String s){
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable transferable = new StringSelection(s);
		clipboard.setContents(transferable,null);
	}
}
