package royalalchemi;

import java.util.ArrayList;

public class CodeGenerator {
	private ArrayList<Part> partsList;
	private int n;

	public String[] GenerateCode(ArrayList<Part> parts, int N) {
		this.n = N;
		this.partsList = parts;
		String HUD_Code = "";
		String Object_Code = "";
		int nOcP = 0;
		for(Part p : partsList){
			if(p.isConstant()){
				nOcP++;
			}
		}
		//If only one part is needed.
		if(partsList.size() == 1 || partsList.size() - nOcP == 1){
			String colors = "";
			for(Texture t : partsList.get(0).getOptions()){
				colors += "\"" + t.getName() + "\", ";
			}
			colors = colors.substring(0,colors.length()-2);
			HUD_Code = "//This code was generated with SL Texture Change Scripter by Nai Alchemi.\n"
					+ "//Place this code in your \"button\" that you want to be clicked when the user wants to change the texture of the product.\n"
					+ "//This generated code is licenced under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License\n"
					+ "//License only covers the generated code, not the textures that the code refrences by UUID or name, these textures are owned by their respective creators.\n"
					+ "default{\n"
					+ "\ttouch(integer num){\n"
					+ "\t\tllDialog(llGetOwner(),\"Select color.\", [" + colors + "], -" + n + ");\n"
					+ "\t}\n"
					+ "\tchanged(integer change){\n"
					+ "\t\tif(change&CHANGED_OWNER){\n"
					+ "\t\t\tllResetScript();\n"
					+ "\t\t}\n"
					+ "\t}\n"
					+ "}";

			String CODE = "";
			for(int i = 0; i < partsList.get(0).getOptions().size(); i++){
				//For else if statements.
				if(i > 0 && i < (partsList.get(0).getOptions().size() - 1)){
					CODE +=   "\t\t}else if(msg == \"" + partsList.get(0).getOption(i).getName() + "\"){\n"
							+ "\t\t\tUUID = \"" + partsList.get(0).getOption(i).getUUID() + "\";\n";
				//For last else if statement.
				}else if(i == (partsList.get(0).getOptions().size() - 1)){
					CODE +=	  "\t\t}else if(msg == \"" + partsList.get(0).getOption(i).getName() + "\"){\n"
							+ "\t\t\tUUID = \"" + partsList.get(0).getOption(i).getUUID() + "\";\n"
							+ "\t\t}\n";
				//For first if statement.
				}else{
					CODE =	  "\t\tif(msg == \"" + partsList.get(0).getOption(0).getName() + "\"){\n"
							+ "\t\t\tUUID = \"" + partsList.get(0).getOption(0).getUUID() + "\";\n";
				}
			}
			for(int p = 0; p < partsList.size(); p++){
				for(int l : partsList.get(p).getLinks()){
					for(int f : partsList.get(p).getFaces()){
						if(!partsList.get(p).isConstant()){
							CODE += "\t\tllSetLinkTexture(" + l + ", UUID, " + f + ");\n";
						}else{
							for(Texture t : partsList.get(p).getOptions()){
								CODE += "\t\tllSetLinkTexture(" + l + ", \"" + t.getUUID() + "\", " + f + ");\n";
							}
						}
					}
				}
			}
			
			Object_Code = "//This code was generated with SL Texture Change Scripter by Nai Alchemi.\n"
					+ "//Place this code inside the object that is going to be changed, which prim dosen't matter unless the Link value LINK_THIS (-4)\n"
					+ "//was used, in which case place inside the object that LINK_THIS is refering too.\n"
					+ "//This generated code is licenced under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License\n"
					+ "//License only covers the generated code, not the textures that the code refrences by UUID or name, these textures are owned by their respective creators.\n"
					+ "key UUID;\n"
					+ "default{\n"
					+ "\tstate_entry(){\n"
					+ "\t\tllListen(-" + n + ", \"\", llGetOwner(),\"\");\n"
					+ "\t}\n"
					+ "\tlisten(integer ch, string name, key id, string msg){\n"
					+ CODE
					+ "\t}\n"
					+ "\tchanged(integer change){\n"
					+ "\t\tif(change&CHANGED_OWNER){\n"
					+ "\t\t\tllResetScript();\n"
					+ "\t\t}\n"
					+ "\t}\n"
					+ "}";
			
		}else{
			//If more then one part is used.
			String CODE = "";
			for(int i = 0; i < partsList.size(); i++){
				
				String colors = "";
				for(Texture t : partsList.get(i).getOptions()){
					colors += "\"" + t.getName() + "\", ";
				}
				colors = colors.substring(0, colors.length() - 2);
				
				if(i > 0 && i < partsList.size()-1){
					//Else if statements.
					CODE += "\t\t}else if(msg == \"" + partsList.get(i).getName() + "\"){\n"
							+ "\t\t\tllDialog(llGetOwner(),\"Select color.\", [" + colors + "], -" + (n+1+i) + ");\n";
				}else if(i == partsList.size()-1){
					//Last else if statement.
					CODE += "\t\t}else if(msg == \"" + partsList.get(i).getName() + "\"){\n"
							+ "\t\t\tllDialog(llGetOwner(),\"Select color.\", [" + colors + "], -" + (n+1+i) + ");\n"
							+ "\t\t}\n";
				}else{
					//First if statement.
					CODE = "\t\tif(msg == \"" + partsList.get(0).getName() + "\"){\n"
							+ "\t\t\tllDialog(llGetOwner(),\"Select color.\", [" + colors + "], -" + (n+1+i) + ");\n";
				}
			}
			HUD_Code = "//This code was generated with SL Texture Change Scripter by Nai Alchemi.\n"
					+ "//Place this code in your \"button\" that you want to be clicked when the user wants to change the texture of the product.\n"
					+ "//was used, in which case place inside the object that LINK_THIS is refering too.\n"
					+ "//This generated code is licenced under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License\n"
					+ "//License only covers the generated code, not the textures that the code refrences by UUID or name, these textures are owned by their respective creators.\n"
					+ "key UUID;\n"
					+ "default{\n"
					+ "\tstate_entry(){\n"
					+ "\t\tllListen(-" + n + ", \"\", llGetOwner(),\"\");\n"
					+ "\t}\n"
					+ "\tlisten(integer ch, string name, key id, string msg){\n"
					+ CODE
					+ "\t}\n"
					+ "\ttouch(integer num){\\n"
					+ "\t\tllDialog(llGetOwner(),\"Select part.\", [parts], -" + n +");\n"
					+ "\t\n}"
					+ "\tchanged(integer change){\n"
					+ "\t\tif(change&CHANGED_OWNER){\n"
					+ "\t\t\tllResetScript();\n"
					+ "\t\t}\n"
					+ "\t}\n"
					+ "}";
			
			CODE = "";
			String listeners = "";
			for(int p = 0; p < partsList.size(); p++){
					listeners += "\t\tllListen(-" + (n + p + 1) + ", \"\", llGetOwner, \"\");\n";
				
				CODE += "\t\tif(ch == -" + (n+p+1) + "){\n";
				for(int o = 0; o < partsList.get(p).getOptions().size(); o++){
					CODE += "\t\t\tif(msg == \"" + partsList.get(p).getOption(o).getName() + "\"){\n"
							+ "\t\t\t\tUUID = \"" + partsList.get(p).getOption(o).getUUID() + "\";\n"
							+ "\t\t\t}\n";
				}
				CODE += "\t\t}\n";
			}
			
			Object_Code = "//This code was generated with SL Texture Change Scripter by Nai Alchemi.\n"
					+ "//Place this code inside the object that is going to be changed, which prim dosen't matter unless the Link value LINK_THIS (-4)\n"
					+ "//was used, in which case place inside the object that LINK_THIS is refering too.\n"
					+ "//This generated code is licenced under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License\n"
					+ "//License only covers the generated code, not the textures that the code refrences by UUID or name, these textures are owned by their respective creators.\n"
					+ "key UUID;\n"
					+ "default{\n"
					+ "\tstate_entry(){\n"
					+ listeners
					+ "\t}\n"
					+ "\tlisten(integer ch, string name, key id, string msg){\n"
					+ CODE
					+ "\t}\n"
					+ "\tchanged(integer change){\n"
					+ "\t\tif(change&CHANGED_OWNER){\n"
					+ "\t\t\tllResetScript();\n"
					+ "\t\t}\n"
					+ "\t}\n"
					+ "}";
		}
		System.out.println(HUD_Code + "\n\n//////////////////////SEPERATOR//////////////////////\n" + Object_Code);
		String[] reVal = new String[2];
		reVal[0] = HUD_Code;
		reVal[1] = Object_Code;
		return reVal;
	}
}
